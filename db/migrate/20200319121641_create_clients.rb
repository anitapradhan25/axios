class CreateClients < ActiveRecord::Migration[6.0]
  def change
    create_table :clients do |t|
      t.string :name
      t.string :image_token
      t.string :url
      t.string :description

      t.timestamps
    end
  end
end
