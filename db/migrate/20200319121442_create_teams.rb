class CreateTeams < ActiveRecord::Migration[6.0]
  def change
    create_table :teams do |t|
      t.string :name
      t.string :designation
      t.string :image_token
      t.string :fb_link
      t.string :instagram_link
      t.string :linked_in

      t.timestamps
    end
  end
end
