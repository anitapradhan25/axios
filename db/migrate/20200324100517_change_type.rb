class ChangeType < ActiveRecord::Migration[6.0]
  def change
    change_column :products, :description, :text
    change_column :clients, :description, :text
  end
end
