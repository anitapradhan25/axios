class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.string :title
      t.string :description
      t.string :link
      t.string :image_token

      t.timestamps
    end
  end
end
