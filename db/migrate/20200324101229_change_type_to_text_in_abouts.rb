class ChangeTypeToTextInAbouts < ActiveRecord::Migration[6.0]
  def change
    change_column :abouts, :description, :text
  end
end
