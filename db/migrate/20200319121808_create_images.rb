class CreateImages < ActiveRecord::Migration[6.0]
  def change
    create_table :images do |t|
      t.string :image_token
      t.string :image_name

      t.timestamps
    end
  end
end
