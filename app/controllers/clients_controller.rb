class ClientsController < ApplicationController
  before_action :set_client, only: [:show, :edit, :update, :destroy]

  # GET /clients
  def index
    @clients = Client.all
  end

  # GET /clients/1
  def show
      clientimage = Image.where(image_token: @client.image_token).first
    if  clientimage.nil?
      @clientimage = ""
    else
      @clientimage =  clientimage.image_name.url(:thumb).to_s 
    end
  end

  # GET /clients/new
  def new
    @client = Client.new
  end

  # GET /clients/1/edit
  def edit
  end

  # POST /clients
  def create
    @client = Client.new(client_params)
    if !params[:image_name].nil?
      @client.image_token = "client"+ DateTime.now.strftime("%Q")
      Image.create(image_token: @client.image_token, image_name: params[:image_name])
    end

    if @client.save
      redirect_to @client,  success: 'Client was successfully created.'
   
    else
      render :new
    end
  end

  # PATCH/PUT /clients/1
  def update
    if @client.update(client_params)
      if !@client.image_token.nil?
        if !params[:image_name].nil?
          Image.where(image_token: @client.image_token).update(image_name: params[:image_name])
        end
      else 
        if !params[:image_name].nil?
          @client.image_token = "client"+ DateTime.now.strftime("%Q") 
          Client.where(id: @client.id).update(image_token: @client.image_token)
          Image.create(image_token: @client.image_token, image_name: params[:image_name])
        end
      end
      redirect_to @client,  success: 'Client was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /clients/1
  def destroy
    if !@client.image_token.nil?
      Image.where(image_token: @client.image_token).delete_all
    end
    @client.destroy
    redirect_to clients_url, danger: 'Client was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client
      @client = Client.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def client_params
      params.require(:client).permit(:name, :image_token, :url, :description)
    end
end
