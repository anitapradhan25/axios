class TeamsController < ApplicationController
  before_action :set_team, only: [:show, :edit, :update, :destroy]

  # GET /teams
  def index
    @teams = Team.all
  end

  # GET /teams/1
  def show
    teamimage = Image.where(image_token: @team.image_token).first
    if teamimage.nil?
      @teamimage = ""
    else
      @teamimage =  teamimage.image_name.url(:thumb).to_s 
    end

  end

  # GET /teams/new
  def new
    @team = Team.new
  end

  # GET /teams/1/edit
  def edit
  end

  # POST /teams
  def create
    @team = Team.new(team_params) 
    if !params[:image_name].nil?
      @team.image_token = "team"+ DateTime.now.strftime("%Q")
      Image.create(image_token: @team.image_token, image_name: params[:image_name])
    end

    if @team.save
      redirect_to @team, success: 'Team was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /teams/1
  def update
    if @team.update(team_params)
      if !@team.image_token.nil?
        if !params[:image_name].nil?
          Image.where(image_token: @team.image_token).update(image_name: params[:image_name])
        end
      else 
        if !params[:image_name].nil?
          @team.image_token = "team"+ DateTime.now.strftime("%Q") 
          Team.where(id: @team.id).update(image_token: @team.image_token)
          Image.create(image_token: @team.image_token, image_name: params[:image_name])
        end
      end
      redirect_to @team, success: 'Team was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /teams/1
  def destroy
    if !@team.image_token.nil?
      Image.where(image_token: @team.image_token).delete_all
    end
    @team.destroy
    redirect_to teams_url, danger: 'Team was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_team
      @team = Team.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def team_params
      params.require(:team).permit(:name, :designation, :image_token, :fb_link, :instagram_link, :linked_in)
    end
end
