class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]

  # GET /products
  def index
    @products = Product.all
  end

  # GET /products/1
  def show
    @productimage = Image.where(image_token: @product.image_token)
  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
  end

  # POST /products
  def create
    @product = Product.new(product_params)
    if !params[:image_name].nil?
      @product.image_token = "product"+ DateTime.now.strftime("%Q")
      params[:image_name].each  do |file| 
      Image.create(image_token: @product.image_token, image_name: file)
      end
    end
    if @product.save
      redirect_to @product, success: 'Product was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /products/1
  def update
    if @product.update(product_params)
      if !@product.image_token.nil?
        if !params[:image_name].nil?
            params[:image_name].each  do |file|     
            Image.create(image_token: @product.image_token, image_name: file)
            end
       end
      else 
         if !params[:image_name].nil?
            @product.image_token = "product"+ DateTime.now.strftime("%Q")
            params[:image_name].each  do |file| 
              Product.where(id: @product.id).update(image_token: @product.image_token)
              Image.create(image_token: @product.image_token, image_name: file)
            end
        end
      end
      redirect_to @product, success: 'Product was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /products/1
  def destroy

    if !@product.image_token.nil?
      Image.where(image_token: @product.image_token).delete_all
    end
    @product.destroy
    redirect_to products_url, danger: 'Product was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_params
      params.require(:product).permit(:title, :description, :link, :image_token)
    end
end
