class ApiController < ApplicationController
    skip_before_action :verify_authenticity_token
    def get_abouts_api
       
        @data= About.select("abouts.id,title,description, image_name").joins('LEfT JOIN images on images.image_token=abouts.image_token').first
        if @data.nil?
            data = " "
        else 
            data = @data
        end

        render json: data
    end

    def get_teams_api
   
        data= Team.select("teams.id,name,designation,fb_link,instagram_link,linked_in,image_name").joins('LEfT JOIN images on images.image_token=teams.image_token')
        render json: data
    end

    def get_products_api
        data =Product.all
        returnHash = Hash.new
        returnArray = Array.new
        data.each do |products|
            returnData = Hash.new
            returnData ["id"] = products.id
            returnData ["title"] = products.title
            returnData ["description"] = products.description
            returnData ["link"] = products.link
            returnimage = Array.new
            Image.where(image_token: products.image_token).each do |images|
                productImage = Hash.new
                productImage["product_image"] = images.image_name.url(:thumb).to_s 
                returnimage.push(productImage)
            end
            returnData["image"] = returnimage
            returnArray.push(returnData)
        end
         returnHash["product"] = returnArray
        render json: returnHash
    end
    

    def get_clients_api
        data = Client.select("clients.id,name,description, url,image_name").joins('LEfT JOIN images on images.image_token=clients.image_token')
        render json: data
    end


end
