class AboutsController < ApplicationController
  before_action :set_about, only: [:show, :edit, :update, :destroy]

  # GET /abouts
  def index
    @abouts = About.all
  end

  # GET /abouts/1
  def show
    aboutimage = Image.where(image_token: @about.image_token).first
    if  aboutimage.nil?
      @aboutimage = ""
    else
      @aboutimage =  aboutimage.image_name.url(:thumb).to_s 
    end
  end

  # GET /abouts/new
  def new
    @about = About.new
  end

  # GET /abouts/1/edit
  def edit
  end

  # POST /abouts
  def create
    if About.count == 0
    @about = About.new(about_params)
    if !params[:image_name].nil?
      @about.image_token = "about"+ DateTime.now.strftime("%Q")
      Image.create(image_token:  @about.image_token, image_name: params[:image_name])
    end
    else
      About.all.each do |about|
        if !about.image_token.nil?
        Image.where(image_token: about.image_token).first.delete
        end
      end
      @about = About.delete_all
      @about = About.new(about_params)
      if !params[:image_name].nil?
        @about.image_token = "about"+ DateTime.now.strftime("%Q")
        Image.create(image_token:  @about.image_token, image_name: params[:image_name])
      end
     
    end
    
    if @about.save
      redirect_to @about, success: 'About was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /abouts/1
  def update
    if @about.update(about_params)
      if !@about.image_token.nil?
        if !params[:image_name].nil?
          Image.where(image_token: @about.image_token).update(image_name: params[:image_name])
        end
      else 
        if !params[:image_name].nil?
          @about.image_token = "about"+ DateTime.now.strftime("%Q") 
          About.where(id: @about.id).update(image_token: @about.image_token)
          Image.create(image_token: @about.image_token, image_name: params[:image_name])
        end
      end
      redirect_to @about, success: 'About was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /abouts/1
  def destroy
    if !@about.image_token.nil?
      Image.where(image_token: @about.image_token).delete_all
    end
    @about.destroy
    redirect_to abouts_url, danger: 'About was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_about
      @about = About.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def about_params
      params.require(:about).permit(:title, :description, :image_token)
    end
end
