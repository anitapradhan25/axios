class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  add_flash_types :success, :danger, :info, :warning 
  before_action :authenticate_user!
  protect_from_forgery with: :exception
end
